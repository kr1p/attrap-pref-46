import os
import datetime

from bs4 import BeautifulSoup
from urllib.parse import unquote

from Attrap import Attrap


class Attrap_pref31(Attrap):

    # Config
    __HOST = 'https://www.haute-garonne.gouv.fr'
    __RAA_PAGE = f'{__HOST}/Publications/Recueil-des-Actes-Administratifs/Recueil-des-Actes-Administratifs-Haute-Garonne'
    __USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'
    full_name = 'Préfecture de la Haute-Garonne'
    short_code = 'pref31'

    def __init__(self, data_dir):
        super().__init__(data_dir, self.__USER_AGENT)
        self.set_sleep_time(30)

    def get_raa(self, keywords):
        # On cherche les pages de chaque mois
        page_content = self.get_page(self.__RAA_PAGE, 'get').content
        month_pages = self.get_sub_pages(
            page_content,
            '.fr-card.fr-card--sm.fr-card--grey.fr-enlarge-link div.fr-card__body div.fr-card__content h2.fr-card__title a',
            self.__HOST,
            False
        )[::-1]

        pages_to_parse = []

        # On filtre les pages de mois pour limiter le nombre de requêtes
        for month_page in month_pages:
            guessed_date = Attrap.guess_date(month_page['name'], '([a-zéû]* [0-9]{4})')
            if guessed_date >= self.not_before.replace(day=1):
                pages_to_parse.append(month_page['url'])

        elements = []
        # On parse les pages des mois qu'on veut analyser
        for element in self.get_raa_with_pager(
            pages_to_parse,
            ".fr-pagination__link.fr-pagination__link--next",
            self.__HOST
        ):
            elements.append(element)

        self.parse_raa(elements, keywords)
        self.mailer()

    def get_raa_elements(self, page_content):
        elements = []
        # On charge le parser
        soup = BeautifulSoup(page_content, 'html.parser')

        # On récupère chaque balise a
        for a in soup.select('div.fr-card__body div.fr-card__content h2.fr-card__title a.fr-card__link.menu-item-link'):
            if a.get('href') and a['href'].endswith('.pdf'):
                if a['href'].startswith('/'):
                    url = f"{self.__HOST}{a['href']}"
                else:
                    url = a['href']

                url = unquote(url)
                name = a.get_text().strip().capitalize()
                date = datetime.datetime.strptime(a['title'].split(' - ')[-1].strip(), '%d/%m/%Y')

                raa = Attrap.RAA(url, date, name)
                elements.append(raa)
        return elements
