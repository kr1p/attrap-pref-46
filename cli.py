#!/usr/bin/env python3

import os
import argparse
import logging
import datetime
import dateparser
import importlib

from Attrap import Attrap

# Config
__KEYWORDS = os.getenv('KEYWORDS') or ''
__DATA_DIR_ROOT = os.path.dirname(os.path.abspath(__file__)) + '/data/'
__SMTP_HOSTNAME = os.getenv('SMTP_HOSTNAME') or 'localhost'
__SMTP_USERNAME = os.getenv('SMTP_USERNAME') or ''
__SMTP_PASSWORD = os.getenv('SMTP_PASSWORD') or ''
__EMAIL_FROM = os.getenv('EMAIL_FROM')
__EMAIL_TO = os.getenv('EMAIL_TO')
if os.getenv('SMTP_PORT'):
    __SMTP_PORT = int(os.getenv('SMTP_PORT'))
else:
    __SMTP_PORT = 587
if os.getenv('SMTP_STARTTLS'):
    __SMTP_STARTTLS = True
else:
    __SMTP_STARTTLS = False
if os.getenv('SMTP_SSL'):
    __SMTP_SSL = True
else:
    __SMTP_SSL = False
if os.getenv('NOT_BEFORE'):
    try:
        relative_date = dateparser.parse(os.getenv('NOT_BEFORE'))
        __NOT_BEFORE = datetime.datetime(year=relative_date.year, month=relative_date.month, day=relative_date.day)
    except Exception as exc:
        __NOT_BEFORE = datetime.datetime.strptime(
            os.getenv('NOT_BEFORE'), '%Y-%m-%d'
        )
else:
    __NOT_BEFORE = datetime.datetime(2024, 1, 1)
__MASTODON_ACCESS_TOKEN = os.getenv('MASTODON_ACCESS_TOKEN')
__MASTODON_INSTANCE = os.getenv('MASTODON_INSTANCE')

# Liste des administrations supportées
available_administrations = [
    'ppparis',
    'pref2b',
    'pref03',
    'pref04',
    'pref05',
    'pref06',
    'pref09',
    'pref10',
    'pref13',
    'pref25',
    'pref31',
    'pref33',
    'pref34',
    'pref35',
    'pref38',
    'pref39',
    'pref42',
    'pref44',
    'pref46',
    'pref59',
    'pref62',
    'pref63',
    'pref64',
    'pref65',
    'pref66',
    'pref69',
    'pref73',
    'pref75',
    'pref80',
    'pref81',
    'pref83',
    'pref87',
    'pref92',
    'pref93',
    'pref94',
    'pref976',
    'prefIdf'
]

# Début du script
parser = argparse.ArgumentParser(
    prog='cli.py',
    description='Télécharge les RAA d\'une administration donnée et recherche des mots-clés'
)
parser.add_argument(
    'administration',
    action='store',
    help='identifiant de l\'administration',
    choices=available_administrations
)
parser.add_argument(
    '-k',
    '--keywords',
    action='store',
    help='liste des termes recherchés, séparés par une virgule (aucun par défaut)'
)
parser.add_argument(
    '--not-before',
    action='store',
    help='n\'analyse pas les RAA datant d\'avant la date indiquée, au format YYYY-MM-DD (par défaut : 2024-01-01)'
)
parser.add_argument(
    '--smtp-hostname',
    action='store',
    help='nom d\'hôte SMTP (par défaut : localhost)'
)
parser.add_argument(
    '--smtp-username',
    action='store',
    help='nom d\'utilisateur SMTP (par défaut : vide)'
)
parser.add_argument(
    '--smtp-password',
    action='store',
    help='mot de passe SMTP (par défaut : vide)'
)
parser.add_argument(
    '--smtp-port',
    action='store',
    help='port SMTP (par défaut : 587)'
)
parser.add_argument(
    '--smtp-starttls',
    action='store_true',
    help='connexion SMTP avec STARTTLS'
)
parser.add_argument(
    '--smtp-ssl',
    action='store_true',
    help='connexion SMTP avec SSL'
)
parser.add_argument(
    '-f',
    '--email-from',
    action='store',
    help='adresse de courrier électronique expéditrice des notifications'
)
parser.add_argument(
    '-t',
    '--email-to',
    action='store',
    help='adresses de courriers électroniques destinataires des notifications (séparées par une virgule)'
)

for administration in available_administrations:
    parser.add_argument(
        f'--{administration}-email-to',
        action='store',
        help=f'adresses de courrier électronique destinataires des notifications (séparées par une virgule) uniquement si l\'analyse concerne {administration} (s\'ajoute à celles précisées dans --email-to)'
    )

parser.add_argument(
    '--mastodon-access-token',
    action='store',
    help='jeton d\'accès pour publier sur Mastodon (par défaut : vide)'
)
parser.add_argument(
    '--mastodon-instance',
    action='store',
    help='URL de l\'instance (doit inclure "http://" ou "https://" ; par défaut : vide)'
)
parser.add_argument(
    '-v',
    action='store_true',
    help='relève le niveau de verbosité à INFO'
)
parser.add_argument(
    '-vv',
    action='store_true',
    help='relève le niveau de verbosité à DEBUG'
)
args = parser.parse_args()

if (args.v or os.getenv('VERBOSE')) and not args.vv and not os.getenv('VVERBOSE'):
    logging.basicConfig(level=logging.INFO)
    logging.getLogger("stem").setLevel(logging.WARNING)

if args.vv or os.getenv('VVERBOSE'):
    logging.basicConfig(level=logging.DEBUG)
    logging.getLogger("stem").setLevel(logging.WARNING)

if args.keywords:
    __KEYWORDS = args.keywords

if args.not_before:
    try:
        relative_date = dateparser.parse(args.not_before)
        __NOT_BEFORE = datetime.datetime(year=relative_date.year, month=relative_date.month, day=relative_date.day)
    except Exception as exc:
        __NOT_BEFORE = datetime.datetime.strptime(args.not_before, '%Y-%m-%d')

if args.smtp_hostname:
    __SMTP_HOSTNAME = args.smtp_hostname

if args.smtp_username:
    __SMTP_USERNAME = args.smtp_username

if args.smtp_password:
    __SMTP_PASSWORD = args.smtp_password

if args.smtp_port:
    __SMTP_PORT = int(args.smtp_port)

if args.smtp_starttls:
    __SMTP_STARTTLS = True

if args.smtp_ssl:
    __SMTP_SSL = True

if args.email_from:
    __EMAIL_FROM = args.email_from

if args.email_to:
    __EMAIL_TO = args.email_to

if args.mastodon_access_token:
    __MASTODON_ACCESS_TOKEN = args.mastodon_access_token

if args.mastodon_instance:
    __MASTODON_INSTANCE = args.mastodon_instance

__DATA_DIR = f'{__DATA_DIR_ROOT}{args.administration}/'

# On calcule la liste des mails à notifier (liste générale EMAIL_TO + liste
# administration EMAIL_TO_ADMINISTRATION**)
__ADMINISTRATION_EMAIL_TO = ''
administration_var_name = f'{args.administration}_EMAIL_TO'.upper()
if os.getenv(administration_var_name):
    __ADMINISTRATION_EMAIL_TO = os.getenv(administration_var_name)
else:
    for arg in vars(args).items():
        if arg[0] == f'{args.administration}_email_to':
            __ADMINISTRATION_EMAIL_TO = arg[1]

if __ADMINISTRATION_EMAIL_TO and not __ADMINISTRATION_EMAIL_TO == '':
    if __EMAIL_TO:
        __EMAIL_TO = f'{__EMAIL_TO},{__ADMINISTRATION_EMAIL_TO}'
    else:
        __EMAIL_TO = __ADMINISTRATION_EMAIL_TO

module = importlib.import_module(f'Attrap_{args.administration}')
attrap = getattr(module, f'Attrap_{args.administration}')(__DATA_DIR)

attrap.not_before = __NOT_BEFORE
attrap.configure_mailer(__SMTP_HOSTNAME, __SMTP_USERNAME, __SMTP_PASSWORD, __SMTP_PORT, __SMTP_STARTTLS, __SMTP_SSL,
                        __EMAIL_FROM, __EMAIL_TO, f'[Attrap] [{attrap.full_name}] Nouveaux éléments trouvés')
attrap.configure_mastodon(__MASTODON_ACCESS_TOKEN, __MASTODON_INSTANCE, f'[{attrap.full_name}]', f'#{attrap.short_code}')
attrap.get_raa(__KEYWORDS)
