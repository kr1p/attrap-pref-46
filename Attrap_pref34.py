import os
import datetime

from bs4 import BeautifulSoup
from urllib.parse import unquote

from Attrap import Attrap


class Attrap_pref34(Attrap):

    # Config
    __HOST = 'https://www.herault.gouv.fr'
    __RAA_PAGE = {
        '2024': f'{__HOST}/Publications/Recueils-des-actes-administratifs/Recueil-des-actes-administratifs-2024',
        '2023': f'{__HOST}/Publications/Recueils-des-actes-administratifs/Recueil-des-actes-administratifs-2023',
        '2022': f'{__HOST}/Publications/Recueils-des-actes-administratifs/Recueil-des-actes-administratifs-2022',
        '2021': f'{__HOST}/Publications/Recueils-des-actes-administratifs/Recueil-des-actes-administratifs-2021',
        '2020': f'{__HOST}/Publications/Recueils-des-actes-administratifs/Recueil-des-actes-administratifs-2020',
        '2019': f'{__HOST}/Publications/Recueils-des-actes-administratifs/Archives/Recueil-des-actes-administratifs-2019'
    }
    __USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'
    full_name = 'Préfecture de l\'Hérault'
    short_code = 'pref34'

    def __init__(self, data_dir):
        super().__init__(data_dir, self.__USER_AGENT)
        self.set_sleep_time(30)

    def get_raa(self, keywords):
        pages_to_parse = []
        if self.not_before.year <= 2024:
            pages_to_parse.append(self.__RAA_PAGE['2024'])
        if self.not_before.year <= 2023:
            pages_to_parse.append(self.__RAA_PAGE['2023'])
        if self.not_before.year <= 2022:
            pages_to_parse.append(self.__RAA_PAGE['2022'])
        if self.not_before.year <= 2021:
            pages_to_parse.append(self.__RAA_PAGE['2021'])
        if self.not_before.year <= 2020:
            pages_to_parse.append(self.__RAA_PAGE['2020'])
        if self.not_before.year <= 2019:
            pages_to_parse.append(self.__RAA_PAGE['2019'])

        elements = []
        for raa_page in pages_to_parse:
            page_content = self.get_page(raa_page, 'get').content
            for element in self.get_raa_elements(page_content):
                elements.append(element)

        self.parse_raa(elements, keywords)
        self.mailer()

    def get_raa_elements(self, page_content):
        elements = []
        # On charge le parser
        soup = BeautifulSoup(page_content, 'html.parser')

        # On récupère chaque balise a
        for a in soup.select('a.fr-link.fr-link--download'):
            if a.get('href') and a['href'].endswith('.pdf'):
                if a['href'].startswith('/'):
                    url = f"{self.__HOST}{a['href']}"
                else:
                    url = a['href']

                url = unquote(url)
                name = a.find('span').previous_sibling.replace('Télécharger ', '').strip()
                date = datetime.datetime.strptime(a.find('span').get_text().split(' - ')[-1].strip(), '%d/%m/%Y')

                raa = Attrap.RAA(url, date, name)
                elements.append(raa)
        return elements
