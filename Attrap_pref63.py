import os
import datetime
import re

from bs4 import BeautifulSoup
from urllib.parse import unquote

from Attrap import Attrap


class Attrap_pref63(Attrap):

    # Config
    __HOST = 'https://www.puy-de-dome.gouv.fr'
    __RAA_PAGE = f'{__HOST}/Publications/Recueils-des-actes-administratifs/Recueils-des-actes-administratifs-Puy-de-Dome'
    __USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0'
    full_name = 'Préfecture du Puy-de-Dôme'
    short_code = 'pref63'

    def __init__(self, data_dir):
        super().__init__(data_dir, self.__USER_AGENT)
        self.set_sleep_time(30)

    def get_raa(self, keywords):
        year_pages_to_parse = []

        # On détermine quelles pages d'année parser
        page_content = self.get_page(self.__RAA_PAGE, 'get').content
        year_pages = self.get_sub_pages(
            page_content,
            'div.fr-card.fr-card--sm.fr-card--grey.fr-enlarge-link div.fr-card__body div.fr-card__content h2.fr-card__title a',
            self.__HOST,
            False
        )
        for year_page in year_pages:
            if not year_page['name'].strip() == 'Archives':
                year = 9999
                try:
                    year = int(year_page['name'].strip())
                except Exception as exc:
                    logger.warning(f"Impossible de deviner l\'année de la page {year_page['name']}")
                    year = 9999

                if year >= self.not_before.year:
                    year_pages_to_parse.append(year_page['url'])

        elements = []
        # Pour chaque année, on parse les RAA
        for year_page in year_pages_to_parse:
            page_content = self.get_page(year_page, 'get').content
            for element in self.get_raa_elements(page_content):
                elements.append(element)

        # On parse les RAA
        self.parse_raa(elements, keywords)
        self.mailer()

    def get_raa_elements(self, page_content):
        elements = []
        # On charge le parser
        soup = BeautifulSoup(page_content, 'html.parser')

        # On récupère chaque balise a
        for a in soup.select('a.fr-link.fr-link--download'):
            if a.get('href') and a['href'].endswith('.pdf'):
                if a['href'].startswith('/'):
                    url = f"{self.__HOST}{a['href']}"
                else:
                    url = a['href']

                url = unquote(url)
                name = a.find('span').previous_sibling.replace('Télécharger ', '').strip()
                date = datetime.datetime.strptime(a.find('span').get_text().split(' - ')[-1].strip(), '%d/%m/%Y')

                raa = Attrap.RAA(url, date, name)
                elements.append(raa)
        return elements
