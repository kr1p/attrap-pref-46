make: ppparis pref2b pref03 pref04 pref05 pref06 pref09 pref10 pref13 pref25 pref31 pref33 pref34 pref35 pref38 pref39 pref42 pref44 pref46 pref59 pref62 pref63 pref64 pref65 pref66 pref69 pref73 pref75 pref80 pref81 pref83 pref87 pref92 pref93 pref94 pref976 prefIdf

ppparis:
	bin/python3 cli.py ppparis
pref2b:
	bin/python3 cli.py pref2b
pref03:
	bin/python3 cli.py pref03
pref04:
	bin/python3 cli.py pref04
pref05:
	bin/python3 cli.py pref05
pref06:
	bin/python3 cli.py pref06
pref09:
	bin/python3 cli.py pref09
pref10:
	bin/python3 cli.py pref10
pref13:
	bin/python3 cli.py pref13
pref25:
	bin/python3 cli.py pref25
pref31:
	bin/python3 cli.py pref31
pref33:
	bin/python3 cli.py pref33
pref34:
	bin/python3 cli.py pref34
pref35:
	bin/python3 cli.py pref35
pref38:
	bin/python3 cli.py pref38
pref39:
	bin/python3 cli.py pref39
pref42:
	bin/python3 cli.py pref42
pref44:
	bin/python3 cli.py pref44
pref46:
	bin/python3 cli.py pref46
pref59:
	bin/python3 cli.py pref59
pref62:
	bin/python3 cli.py pref62
pref63:
	bin/python3 cli.py pref63
pref64:
	bin/python3 cli.py pref64
pref65:
	bin/python3 cli.py pref65
pref66:
	bin/python3 cli.py pref66
pref69:
	bin/python3 cli.py pref69
pref73:
	bin/python3 cli.py pref73
pref75:
	bin/python3 cli.py pref75
pref80:
	bin/python3 cli.py pref80
pref81:
	bin/python3 cli.py pref81
pref83:
	bin/python3 cli.py pref83
pref87:
	bin/python3 cli.py pref87
pref92:
	bin/python3 cli.py pref92
pref93:
	bin/python3 cli.py pref93
pref94:
	bin/python3 cli.py pref94
pref976:
	bin/python3 cli.py pref976
prefIdf:
	bin/python3 cli.py prefIdf
lint:
	bin/pycodestyle --first --show-source --ignore=E501 *.py
